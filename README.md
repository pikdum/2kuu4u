# 2kuu4u

a nodejs rewrite of kuu2

## future work

- rework database schema in postgresql
- migrate data
- rewrite frontend using pug
  - something other than vue for the dynamic bits
  - maybe react, more likely just plain javascript
- try adding a captcha of some sort
  - maybe a rate limiter, as well
  - some way to IP ban would be nice
- add user identifiers
- admin login, using auth0
