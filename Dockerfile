FROM docker.io/node:16
ARG TARGETARCH
WORKDIR /app/
EXPOSE 3000
CMD /app/entrypoint.sh

RUN wget -q https://github.com/benbjohnson/litestream/releases/download/v0.3.6/litestream-v0.3.6-linux-${TARGETARCH}-static.tar.gz && tar xf *.tar.gz && mv litestream /usr/local/bin/ && rm *.tar.gz
COPY package* ./
RUN npm ci
COPY . .
