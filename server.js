const { Sequelize, Model, DataTypes } = require("sequelize");
const wordwrap = require("wordwrapjs");
const figlet = require("figlet");
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const nunjucks = require("nunjucks");

const app = express();
const port = process.env.PORT || 3000;
const host = process.env.HOST || "0.0.0.0";

const env = nunjucks.configure("views", {
  autoescape: true,
  express: app,
});

env.addFilter("wrap", (str) => {
  return wordwrap.wrap(str, { width: 80, break: true });
});

env.addFilter("allfiglet", (str) => {
  let newStr = "";
  for (f of figlet.fontsSync()) {
    newStr = newStr + figlet.textSync(str, { font: f }) + " - " + f + "\n";
  }
  return newStr;
});

env.addFilter("figlet", (str, font) => {
  font = font || "Big";
  return figlet.textSync(str, { font: font });
});

const sequelize = new Sequelize({
  dialect: "sqlite",
  storage: process.env.DB_PATH || "./kuudere.db",
  define: {
    timestamps: false,
  },
});

sequelize.query("PRAGMA journal_mode=WAL;");

app.use(cors());
app.use(bodyParser.json());
app.use(express.static("public"));

const Post = sequelize.define("post", {
  post_id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  subject: { type: Sequelize.TEXT },
  name: { type: Sequelize.TEXT },
  data: { type: Sequelize.TEXT },
  parent: { type: Sequelize.INTEGER },
  timestamp: { type: Sequelize.TEXT },
  bumptime: { type: Sequelize.TEXT },
  locked: { type: Sequelize.INTEGER },
  sticky: { type: Sequelize.INTEGER },
});

app.get("/", (req, res) => res.render("index.html"));
app.get("/forum/", (req, res) => {
  Post.findAll({
    where: { parent: 0 },
    order: [["bumptime", "DESC"]],
  })
    .then((threads) => res.render("threads.html", { threads }))
    .catch((error) => res.status(400).send(error));
});
app.get("/forum/:id/", (req, res) => {
  let threadFilter = {
    [Sequelize.Op.or]: [{ post_id: req.params.id }, { parent: req.params.id }],
  };
  Post.findAll({
    where: threadFilter,
  })
    .then((posts) => res.render("thread.html", { posts }))
    .catch((error) => res.status(400).send(error));
});
app.get("/health/", (req, res) => res.send({ status: "success" }));

app.get("/thread/", (req, res) => {
  Post.findAll({
    where: { parent: 0 },
    order: [["bumptime", "DESC"]],
    attributes: {
      include: [
        [
          sequelize.literal(`(
                        SELECT COUNT(*)
                        FROM posts AS p
                        WHERE
                        p.parent = post.post_id
                    )`),
          "replycount",
        ],
      ],
    },
  })
    .then((threads) => res.status(200).send(threads))
    .catch((error) => res.status(400).send(error));
});

app.get("/thread/:id/", (req, res) => {
  let threadFilter = {
    [Sequelize.Op.or]: [{ post_id: req.params.id }, { parent: req.params.id }],
  };
  let sinceFilter = req.query.since
    ? { post_id: { [Sequelize.Op.gt]: req.query.since } }
    : {};
  Post.findAll({
    where: { ...threadFilter, ...sinceFilter },
  })
    .then((posts) => res.status(200).send(posts))
    .catch((error) => res.status(400).send(error));
});

app.post("/thread/:parent/", (req, res) => {
  for (const [key, value] of Object.entries(req.body)) {
    req.body[key] = value.trim();
  }
  let parent = parseInt(req.params.parent);
  req.body.subject = req.body.subject ? req.body.subject : "";
  req.body.name = req.body.name.length > 0 ? req.body.name : "rei";
  if (
    req.body.name.length > 30 ||
    req.body.data.length > 10000 ||
    req.body.data.length == 0 ||
    req.body.subject.length > 30
  ) {
    res.status(400).send("Bad request.");
    return;
  }
  if (parent == 0 && req.body.subject.length == 0) {
    res.status(400).send("Bad request.");
    return;
  }

  // current utc time in yyyy/MM/dd HH:mm:ss
  let date = new Date(new Date().toUTCString().substr(0, 25))
    .toISOString()
    .substr(0, 19)
    .replace(/-/g, "/")
    .replace("T", " ");

  Post.update(
    { bumptime: date },
    {
      where: {
        post_id: parent,
      },
    }
  );

  Post.create({
    subject: req.body.subject,
    parent: parent,
    name: req.body.name,
    data: req.body.data,
    timestamp: date,
    bumptime: date,
  })
    .then((post) => res.status(200).send(post))
    .catch((error) => res.status(400).send(error));
});

app.listen(port, host, () => console.log(`Listening on ${host}:${port}!`));
