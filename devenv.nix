{ pkgs, ... }:

{
  # https://devenv.sh/packages/
  packages = [ pkgs.git ];

  # https://devenv.sh/languages/
  languages.nix.enable = true;
  languages.javascript.enable = true;
  languages.javascript.package = pkgs.nodejs-16_x;

  # https://devenv.sh/pre-commit-hooks/
  pre-commit.hooks.shellcheck.enable = true;
  pre-commit.hooks.prettier.enable = true;

  # https://devenv.sh/processes/
  processes.backend.exec = "npx nodemon server.js";
}
