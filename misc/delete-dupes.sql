DELETE FROM posts WHERE post_id IN (
    SELECT max(post_id) as post_id
    FROM posts
    GROUP BY data, parent, name
    HAVING COUNT(*) > 1
    AND 100000 * (julianday(replace(max(timestamp), '/', '-')) - julianday(replace(min(timestamp), '/', '-'))) < 10
    ORDER BY post_id ASC
);
