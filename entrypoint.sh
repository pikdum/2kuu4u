#!/usr/bin/env bash
set -e

if [ -f "$DB_PATH" ]; then
    echo "Database already exists, skipping restore"
else
    echo "No database found, restoring from replica if exists"
    litestream restore -v -if-replica-exists -o "$DB_PATH" "$LITESTREAM_CONFIG"
fi

exec litestream replicate -exec "node server.js" "$DB_PATH" "$LITESTREAM_CONFIG"
